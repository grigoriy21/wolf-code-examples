<?php

namespace App\Http\Controllers\Admin;

use App\Entity\Product;
use App\Jobs\AddProduct;
use App\Models\Param;
use App\Models\RefValue;
use App\Http\Controllers\Controller;
use \Exception;

class CatalogUploaderController extends Controller
{
    private function checkParams($xml)
    {
        $errors = [];
        $params = Param::all()->keyBy('id');

        foreach ($xml->product as $product) {
            foreach ($product->params->param as $param) {
                $paramId = (int)$param->attributes()->id;
                $value = (string)$param;

                if (!$paramId) {
                    $errors[] = 'Не верно указан ID параметра';
                    continue;
                } elseif (!$params->contains('id', $paramId)) {
                    $errors[] = "Параметр ID:{$paramId} не найден";
                    continue;
                }

                switch ($params[$paramId]->type) {
                    case 'INT':
                        if (!is_numeric($value) || (int)$value != $value) {
                            $errors[] = "Не корректное значение для параметра ID:{$paramId}";
                        }
                        break;

                    case 'FLOAT':
                        if (!is_numeric($value) || (float)$value != $value) {
                            $errors[] = "Не корректное значение для параметра ID:{$paramId}";
                        }
                        break;

                    case 'REF':
                        $refValue = RefValue::where('ref_id', $params[$paramId]->ref_id)->where('id', (int)$value)->first();
                        if (!$refValue) {
                            $errors[] = "Не корректное значение для параметра ID:{$paramId} / {$value}";
                        }
                        break;

                    default:
                        $errors[] = "Ошибка типа параметра ID:{$paramId}";
                        break;
                }
            }
        }

        return $errors;
    }

    public function index()
    {
        $hash = [];
        foreach (scandir(public_path('img101')) as $file) {
            if($file != '.' && $file != '..') {
                $h = hash_file('sha256', public_path("img101/{$file}"));
                if(in_array($h, $hash)) {
                    unlink(public_path("img101/{$file}"));
                } else {
                    $hash[] = $h;
                }
            }
        }

        $l = [];

        try {
            $xml = simplexml_load_file(public_path("iek_lamps.xml"));
            $check = $this->checkParams($xml);
            if ($check) { //проверить заголовок, картинки файлы и пр. лабуду по необхоидмости
                dd($check);
            }

            foreach ($xml->product as $p) {
                $product = new Product();
                $product->setCatalogId(21);
                $product->setManufacturerId(4);
                $product->setTitle((string)$p->title);
                $product->setArticle((string)$p->article);
                $product->setDescription((string)$p->description);
                $product->setImages((array)$p->images->image);

                foreach ($p->files->file as $file) {
                    if (!in_array((string)$file, $l)) {
                        $l[] = (string)$file;

                        $product->addFile([
                            'group_id' => (int)$file->attributes()->group_id ?? null,
                            'title' => (string)$file->attributes()->title,
                            'link' => (string)$file,
                        ]);
                    }
                }
                dispatch(new AddProduct($product));
            }

        } catch (Exception $e) {
            dump($e->getCode(), $e->getMessage());
        }

    }
}
